#include "../include/NTestClientSFTP.h"

// -------------------------
// namespace NTestClientSFTP
// -------------------------

/**
 * Point d'entree
 *
 * @param argc
 * 		Le nombre d'arguments
 * @param argv
 * 		Le vecteur d'arguments
 *
 * @return EXIT_SUCCESS si tout va bien, EXIT_FAILURE sinon
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Client SFTP
	NClientSFTP *sftp;

	// Contenu fichier
	char *contenuFichier;

	// Donnee
	NData *data;

	// Liste fichiers
	NListe *listeFichier;

	// Fichier
	NFichierBinaire *fichier;

	// Iterateur
	NU32 i;

	// Nouveau lien
	char nouveauLien[ 2048 ];

	// Authentification
	NMethodeAuthentificationSSH *authentification;

	// Creer la methode d'authentification
	authentification = NLib_Module_SSH_NMethodeAuthentificationSSH_Construire( "sftp",
		"Lucas310894" );

	// Creer client
	if( ( sftp = NLib_Module_SSH_NClientSFTP_Construire( "127.0.0.1",
		22,
		authentification ) ) != NULL )
	{
		// Test envoi fichier sur disque
		NLib_Module_SSH_NClientSFTP_EnvoyerFichier( sftp,
			"e.json",
			"/www/ExempleFichier.json",
			NTRUE );

		// Test envoi fichier en memoire
		if( !( fichier = NLib_Fichier_NFichierBinaire_ConstruireLecture( "f.json" ) ) )
			return EXIT_FAILURE;
		contenuFichier = NLib_Fichier_NFichierBinaire_LireTout( fichier );
		NLib_Module_SSH_NClientSFTP_EnvoyerFichier2( sftp,
			contenuFichier,
			NLib_Fichier_NFichierBinaire_ObtenirTaille( fichier ),
			"/www/ExempleMemoire.json",
			NTRUE );
		NFREE( contenuFichier );
		NLib_Fichier_NFichierBinaire_Detruire( &fichier );

		// Suppression repertoire
		NLib_Module_SSH_NClientSFTP_SupprimerRepertoire( sftp,
			"/www/mdr" );

		// Creation repertoire
		NLib_Module_SSH_NClientSFTP_CreerRepertoire( sftp,
			"/www/mdr" );

		// Recuperer un fichier sur disque
		NLib_Module_SSH_NClientSFTP_RecevoirFichier( sftp,
			"/www/ExempleMemoire.json",
			"TEST_SFTP.json",
			NTRUE );

		// Recuperer un fichier en memoire
		data = NLib_Module_SSH_NClientSFTP_RecevoirFichier2( sftp,
			"/www/ExempleFichier.json" );
		puts( "Contenu fichier distant:" );
		NLib_Memoire_Afficher( NLib_Memoire_NData_ObtenirData( data ),
			NLib_Memoire_NData_ObtenirTailleData( data ) );
		NLib_Memoire_NData_Detruire( &data );

		// Lister fichiers
		listeFichier = NLib_Module_SSH_NClientSFTP_ListerFichierRepertoire( sftp,
			"/www/" );
		puts( "Dans le dossier /www:" );
		for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( listeFichier ); i++ )
		{
			// Obtenir element
			contenuFichier = NLib_Memoire_NListe_ObtenirElementDepuisIndex( listeFichier,
				i );

			// Nouveau lien
			snprintf( nouveauLien,
				2048,
				"/www/%s",
				contenuFichier );

			// Afficher
			printf( "- %s (%s)\n",
				contenuFichier,
				NLib_Module_SSH_NClientSFTP_EstFichier( sftp,
					nouveauLien ) ?
					"FICHIER"
					: "REPERTOIRE" );
		}
		NLib_Memoire_NListe_Detruire( &listeFichier );

		// Telecharger tout
		NLib_Module_SSH_NClientSFTP_TelechargerTousFichierRepertoire( sftp,
			"/www",
			"www",
			NTRUE,
			NTRUE );

		// Detruire client
		NLib_Module_SSH_NClientSFTP_Detruire( &sftp );
	}

	// OK
	return EXIT_SUCCESS;
}